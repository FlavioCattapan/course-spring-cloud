package com.in28minutes.rest.webservcies.restfulwebservices.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

@Component
public class UserDaoService {

	private static List<User> users = new ArrayList<>();

	private Integer usersConunt = 3;

	static {
		users.add(new User(1, "Flávio", new Date()));
		users.add(new User(2, "Even", new Date()));
		users.add(new User(3, "Jack", new Date()));
	}

	public static List<User> findAll() {
		return users;
	}

	public User save(User user) {
		if (user.getId() == null) {
			user.setId(++usersConunt);
		}
		users.add(user);
		return user;
	}

	public User findOne(Integer id) {
		Optional<User> findUser = users.stream().filter(user -> user.getId().equals(id)).findFirst();
		if (findUser.isPresent()) {
			return findUser.get();
		} else {
			return null;
		}
	}

	public User delete(Integer id) {
		Optional<User> user = users.stream().filter(usr -> usr.getId().equals(id)).findFirst();
		if (user.isPresent()) {
			users.removeIf(usr -> usr.getId().equals(id));
			return user.get();
		} else {
			return null;
		}
	}

}
