package com.microservices.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.RequestContext;

import com.microservices.proxy.CurrencyExchangeServiceProxy;

@RestController
public class CurrencyConversionController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CurrencyExchangeServiceProxy currencyConversionController;

	@GetMapping("/currency-converter/from/{from}/to/{to}/quantity/{quantity}")
	public CurrencyConversionBean convertCurrency(@PathVariable String from, @PathVariable String to, @PathVariable BigDecimal quantity) {
	 
		Map<String , String> map = new HashMap<>();
		map.put("from", from);
		map.put("to", to);
		
		ResponseEntity<CurrencyConversionBean> response =  new RestTemplate().getForEntity("http://127.0.0.1:8000/currency-exchange/from/{from}/to/{to}",CurrencyConversionBean.class, map);
		
		CurrencyConversionBean currencyConversionBean = response.getBody();
		
		return new CurrencyConversionBean(currencyConversionBean.getId(), from, to, BigDecimal.ONE, quantity, quantity.multiply(currencyConversionBean.getConversionMultiple()), currencyConversionBean.getPort());	
	}
	
	@GetMapping("/currency-converter-feign/from/{from}/to/{to}/quantity/{quantity}")
	public CurrencyConversionBean convertCurrencyFeign(@PathVariable String from, @PathVariable String to, @PathVariable BigDecimal quantity) {
		
		logger.info("request -> {} {} request uri -> convertCurrencyFeign", to, from);
	 
		CurrencyConversionBean currencyConversionBean = currencyConversionController.retrieveExchangeValue(from, to);
		
		return new CurrencyConversionBean(currencyConversionBean.getId(), from, to, BigDecimal.ONE, quantity, quantity.multiply(currencyConversionBean.getConversionMultiple()), currencyConversionBean.getPort());	
	}

}
