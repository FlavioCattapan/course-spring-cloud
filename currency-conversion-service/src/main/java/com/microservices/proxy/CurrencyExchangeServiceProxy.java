package com.microservices.proxy;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import com.microservices.controller.CurrencyConversionBean;

//@FeignClient(name = "currency-exchange", url = "localhost:8000")
//@FeignClient(name = "currency-exchange-service") // Usando apenas o eureka
@FeignClient(name = "netflix-zull-api-gateway-server") // enviando pelo Zull
@RibbonClient(name = "currency-exchange-service")
public interface CurrencyExchangeServiceProxy {
	
	// Usando apenas o eureka
	//@GetMapping("/currency-exchange/from/{from}/to/{to}")
	//public CurrencyConversionBean retrieveExchangeValue(@PathVariable String from, @PathVariable String to);
	
	// Usando o Zull
	@GetMapping("/currency-exchange-service/currency-exchange/from/{from}/to/{to}")
	public CurrencyConversionBean retrieveExchangeValue(@PathVariable String from, @PathVariable String to);

}
